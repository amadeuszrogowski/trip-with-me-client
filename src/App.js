import React from 'react'
import TripBar from './components/TripBar'
import Container from '@material-ui/core/Container'
import './App.css'
import axios from 'axios'
import {Route, Switch, withRouter} from 'react-router-dom'
import LoginView from './components/views/LoginView'
import SignUpView from './components/views/SignUpView'
import ProfileForm from './components/forms/ProfileForm'
import ProfileView from './components/views/ProfileView'
import CreateTripForm from './components/forms/CreateTripForm'
import SearchView from './components/views/SearchView'
import TripView from './components/views/TripView'
import BoxView from "./components/views/BoxView";
import EditTripForm from './components/forms/EditTripForm'
import {SERVER_HOST} from "./commons";

class App extends React.Component {

    onSignUpFormSubmit = (e) => {
        e.preventDefault();

        const form = e.target;
        const formData = new FormData(form);
        let jsonObject = {};

        for (const [key, value] of formData.entries()) {
            jsonObject[key] = value;
        }

        console.log(jsonObject);

        axios.post(`http://${SERVER_HOST}:8080/api/auth/signup`,
            jsonObject, {
                headers: {
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                this.props.history.push('/login');
            })
            .catch(error => {
                console.log(error)
            })
    };

    onLoginFormSubmit = (e) => {
        e.preventDefault();

        const form = e.target;
        const data = new FormData(form);
        for (let name in data.keys()) {
            data.set(name, data.get(name))
        }

        console.log(...data);
        axios.post(`http://${SERVER_HOST}:8080/api/auth/signin`,
            data, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                    console.log(response.data)
                    localStorage.setItem('accessToken', response.data.accessToken);
                    localStorage.setItem('userId', JSON.stringify(response.data.userId));
                    localStorage.setItem('profileId', JSON.stringify(response.data.profileId));
                    localStorage.setItem('isAuth', JSON.stringify(true));
                    this.props.history.push('/trips')
                }
            )
            .catch(error => {
                console.log(error);
                return null
            });
    };

    renderLoginView() {
        this.onLogout();
        return <LoginView onSubmit={this.onLoginFormSubmit}/>
    }

    onLogout() {
        localStorage.removeItem('isAuth');
        localStorage.removeItem('userId');
        localStorage.removeItem('profileId');
        localStorage.removeItem('accessToken');
    }

    render() {
        return (
            <React.Fragment>
                <TripBar/>
                <Switch>
                    <Container className={"container"}>
                        <Route exact path="/login" render={() => this.renderLoginView()}/>
                        <Route exact path="/signup" render={() => <SignUpView onSubmit={this.onSignUpFormSubmit}/>}/>
                        <Route exact path="/box" component={BoxView}/>
                        <Route exact path="/trips" component={SearchView}/>
                        <Route exact path="/trips/create" component={CreateTripForm}/>
                        <Route exact path="/trip/:id" component={TripView}/>
                        <Route exact path="/trip/:id/edit" component={EditTripForm}/>
                        <Route exact path="/profile/:id" component={ProfileView}/>
                        <Route exact path="/profile/:id/edit" component={ProfileForm}/>
                    </Container>
                </Switch>
            </React.Fragment>
        );
    }
}

export default withRouter(App)