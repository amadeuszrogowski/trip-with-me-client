import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import {BrowserRouter} from 'react-router-dom'

const routes = (
    <BrowserRouter>
        <App/>
    </BrowserRouter>
);

ReactDOM.render(routes, document.getElementById('root'));