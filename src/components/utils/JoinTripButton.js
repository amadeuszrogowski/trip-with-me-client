import React from 'react'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

const JoinTripButton = (props) => {

    return <Grid item>
        <Paper>
            <Grid item container justify="center">
                <Button onClick={props.onClick}
                        type={props.type || null}
                        disabled={props.joinTripState !== 'PARTICIPATE_IN'}
                        color="primary">
                    <Typography> {props.joinTripState || ''} </Typography>
                </Button>
            </Grid>
        </Paper>
    </Grid>;
};

export default JoinTripButton
