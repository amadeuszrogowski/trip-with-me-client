import React from 'react'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

const CustomButton = (props) => {

    return <Grid item>
        <Paper>
            <Grid item container justify="center">
                <Button type={props.type || null} color="primary">
                    <Typography> {props.msg} </Typography>
                </Button>
            </Grid>
        </Paper>
    </Grid>;
};

export default CustomButton
