import React from 'react'
import axios from 'axios'
import TripOverview from './TripOverview'
import Grid from '@material-ui/core/Grid'
import {SERVER_HOST} from "../../commons";

class TripOverviews extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fetchedTrips: [],
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props !== prevProps) {
            this.fetchData()
        }
    }

    fetchData() {
        axios.get(`http://${SERVER_HOST}:8080/api/trips`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            },
            params: {
                sortStrategy: this.props.sortStrategy,
                ...this.props.filterBy
            }
        })
            .then(response => {
                this.setState({
                    fetchedTrips: response.data
                })
            })
    }

    render() {
        return this.state.fetchedTrips &&
            this.state.fetchedTrips
                .map(trip => <TripOverview key={trip.id} data={trip}/>)
    }
}

export default TripOverviews