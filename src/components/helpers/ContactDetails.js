import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import PhoneIcon from '@material-ui/icons/Phone'
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail'
import WebIcon from '@material-ui/icons/Web'

const ContactDetails = (props) => {

    return <Grid item container>
        <Grid item container justify="center" alignItems="center">
            <Typography variant={"h6"}> Contact details </Typography>
        </Grid>
            <Grid item container justify="center" alignItems="center" direction="row">
                <Grid item container justify="center" alignItems="center">
                    <PhoneIcon/>
                    <Typography> {props.phoneNumber} </Typography>
                </Grid>
                <Grid item container justify="center" alignItems="center">
                    <AlternateEmailIcon/>
                    <Typography> {props.email} </Typography>
                </Grid>
                <Grid item container justify="center" alignItems="center">
                    <WebIcon/>
                    <Typography> {props.facebook} </Typography>
                </Grid>
            </Grid>
    </Grid>
};

export default ContactDetails