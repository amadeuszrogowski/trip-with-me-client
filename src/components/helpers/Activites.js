import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import SmokeFreeIcon from '@material-ui/icons/SmokeFree'
import FilterHdrIcon from '@material-ui/icons/FilterHdr'
import PoolIcon from '@material-ui/icons/Pool'
import PetsIcon from '@material-ui/icons/Pets'
import NatureIcon from '@material-ui/icons/Nature'

class Activites extends React.Component {

    constructor(props) {
        super(props);
        this.iconMap = new Map();
        this.iconMap.set("Don't smoke", <SmokeFreeIcon/>);
        this.iconMap.set("Hiking", <FilterHdrIcon/>);
        this.iconMap.set("Sea", <PoolIcon/>);
        this.iconMap.set("Tents", <NatureIcon/>);
        this.iconMap.set("Survival", <PetsIcon/>);
    };

    getIconFromDesc(desc) {
        return this.iconMap.get(desc);
    }

    render() {
        const {title, interests} = this.props

        return <Grid item container>
            <Grid item>
                <Typography variant={"h6"}> {title} </Typography>
            </Grid>
            <Grid item xs={12} container direction={"row"}>
                    {(interests && interests.length > 1) ? interests.map(interest => (
                        <>
                           {this.getIconFromDesc("Tents")}
                            <Typography>{interest.name || interest}</Typography>
                        </>
                    )) : <Grid item>
                        <Typography> None </Typography>
                    </Grid>}
            </Grid>
        </Grid>
    };
}

export default Activites