import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

const Description = (props) => {

    return <Grid item container direction={"column"}>
        <Grid item>
            <Typography variant={"h6"}> {props.title} </Typography>
        </Grid>
        <Grid item>
                <Typography> {props.desc ? props.desc : "None"} </Typography>
        </Grid>
    </Grid>;
};

export default Description