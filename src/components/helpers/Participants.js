import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import {Link} from "react-router-dom";
import {times} from "lodash";

const Participants = (props) => {

    return (
        <Grid item container direction="column">
            <Grid item>
                <Typography variant={'h6'}> Participants </Typography>
            </Grid>
            <Grid item container direction="row" spacing={3}>

                {props.participants && props.participants.map(participant => {
                    return <Grid item justify="center" alignItems="center" direction="row">
                        <Link to={`/profile/${participant.id}`}>
                            <Avatar alt="Remy Sharp"
                                    src={participant.img}
                            />
                        </Link>

                        <Typography> {participant.name} {participant.surname} </Typography>
                    </Grid>
                })}

                {times(props.participants && props.sizeLimit && (props.sizeLimit - props.participants.length), i => {
                    return <Grid item justify="center" alignItems="center" direction="row">
                        <Avatar alt="Remy Sharp"
                                src="https://mcfarlinumc.org/wp-content/uploads/2018/08/question-mark-scribble-animation-doodle-cartoon-4k_bdi1p4a_e_thumbnail-full01.png"
                        />
                        <Typography> Empty place </Typography>
                    </Grid>
                })}
            </Grid>
        </Grid>
    )
}

export default Participants
