import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import './TripOverview.css'
import {Link} from 'react-router-dom'
import {AttachMoney, DateRange, Group} from '@material-ui/icons'

const TripOverview = (props) => {

    const data = props.data

    return <Grid item container direction={'row'} className={'trip-overview'}>
            <Grid item container spacing={2}>
                <Grid item>
                    <Link to={`/trip/${data.id}`}>
                        <img
                            className={'trip-photo'}
                            src={data.img ? data.img : null}
                            alt={'Mex'}/></Link>
                </Grid>
                <Grid item xs={12} sm container direction={'row'}>
                    <Grid item xs container direction="column" spacing={2}>
                            <Typography variant="h5"> {data.title} </Typography>
                            <Typography>
                                {
                                    data.destinations && (
                                        data.destinations.type === 'cities' ?
                                            data.destinations.places.map(place => `${place.name},${place.country.name} `)
                                            : data.destinations.countries.map(country => `${country.name} `)
                                    )
                                }
                            </Typography>
                            <Typography variant="body2" color="textSecondary" className={'description'}>
                                {data.description}
                            </Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="subtitle1"> <AttachMoney/> {data.budget} </Typography>
                        <Typography variant="subtitle1"> <DateRange/> {data.startDate} - {data.finishDate} </Typography>
                        <Typography variant="subtitle1">
                            <Group/> {`${data.participants.length || '0'}/${data.sizeLimit} `}</Typography>
                    </Grid>
                </Grid>
            </Grid>
    </Grid>
}

export default TripOverview