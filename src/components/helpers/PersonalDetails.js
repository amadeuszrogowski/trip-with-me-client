import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import WorkIcon from '@material-ui/icons/Work'
import SchoolIcon from '@material-ui/icons/School'
import WcIcon from '@material-ui/icons/Wc'
import CakeIcon from '@material-ui/icons/Cake'

const PersonalDetails = (props) => {

    const getAge = (birthDate) => Math.floor((new Date() - new Date(birthDate).getTime()) / 3.15576e+10);

    return <Grid item xs={6} container direction="column">
        <Grid item container>
            <Typography variant={"h6"}> Personal details </Typography>
        </Grid>
            <Grid item container direction="row">
                <CakeIcon/>
                <Typography>{getAge(props.dateOfBirth) || "No provided"} </Typography>
            </Grid>
            <Grid item container direction="row">
                <WcIcon/>
                <Typography>{props.gender || "No provided"}</Typography>
            </Grid>
            <Grid item container direction="row">
                <WorkIcon/>
                <Typography> {props.course || 'No provided'} </Typography>
            </Grid>
            <Grid item container direction="row">
                <SchoolIcon/>
                <Typography> {props.university || 'No provided'} </Typography>
            </Grid>
    </Grid>

};

export default PersonalDetails