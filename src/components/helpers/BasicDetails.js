import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import PersonIcon from '@material-ui/icons/Person'
import LocationOnIcon from '@material-ui/icons/LocationOn'

const BasicDetails = (props) => {

    return <Grid item container>

        <Grid item container justify="center" alignItems="center">
            <Typography variant={"h6"}> Basic details </Typography>
        </Grid>

        <Grid item container direction="row">
            <Grid item container justify="center" alignItems="center">
                <Avatar src={props.img}/>
            </Grid>
            <Grid item container justify="center" alignItems="center" direction="row">
                <PersonIcon/>
                <Typography> {props.name} {props.surname} </Typography>
            </Grid>
            <Grid item container justify="center" alignItems="center" direction="row">
                <LocationOnIcon/>
                <Typography>{props.location}</Typography>
            </Grid>
            <Grid item container justify="center" alignItems="center" direction="row">
                <Typography> Member since 2018</Typography>
            </Grid>
        </Grid>
    </Grid>

};

export default BasicDetails