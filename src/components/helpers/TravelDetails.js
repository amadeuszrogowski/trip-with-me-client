import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import LanguageIcon from '@material-ui/icons/Language'
import PublicIcon from '@material-ui/icons/Public'

const TravelDetails = (props) => {

    const renderCollection = (collection) => {
        return <Grid item>
            {collection ? collection
                .map((element) => <Typography key={element}> {element} </Typography>) : null}
        </Grid>;
    };

    return (
        <Grid item xs={6} container direction="column" spacing={1}>
            <Grid item>
                <Typography variant={"h6"}> Travel details </Typography>
            </Grid>
                <Grid item container direction={"row"}>
                    <LanguageIcon/>
                    {renderCollection(props.languages)}
                </Grid>
                <Grid item container direction={"row"}>
                    <PublicIcon/>
                    {renderCollection(props.countries)}
                </Grid>
        </Grid>
    );
};

export default TravelDetails