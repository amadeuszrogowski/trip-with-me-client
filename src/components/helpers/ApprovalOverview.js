import React from "react";
import axios from "axios";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import {SERVER_HOST} from "../../commons";

class ApprovalOverview extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            profile: {},
            trip: {}
        }
    }

    componentDidMount() {
        axios.get(`http://${SERVER_HOST}:8080/api/utils/approvaloverview`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            },
            params: {
                tripId: this.props.tripId,
                participantId: this.props.participantId
            }
        })
            .then(response => {
                this.setState({
                    profile: response.data.profile,
                    trip: response.data.trip
                })
            });
    }

    render() {
        const {profile, trip} = this.state;
        const props = this.props;

        return <React.Fragment>
            <Grid item>
                <Paper>
                    <Link
                        to={`/profile/${profile.id}`}>
                        <Avatar src={profile.img}/>
                        <Typography> {profile.name} {profile.surname} </Typography>
                    </Link>
                    {
                        profile.contactInfo &&
                        <React.Fragment>
                            <Typography> {profile.contactInfo.phoneNumber} </Typography>
                            <Typography> {profile.contactInfo.emailAddress} </Typography>
                            <Typography> {profile.contactInfo.facebookAccountUrl} </Typography>
                        </React.Fragment>
                    }
                    <Link to={`/trip/${trip.id}`}>
                        <Avatar src={trip.img}/>
                        <Typography> {trip.title} </Typography>
                    </Link>
                    <Button onClick={() => props.onDecline(props.participantId, props.tripId)} color="primary">
                        <Typography> {"Decline"} </Typography>
                    </Button>
                    <Button onClick={() => props.onApprove(props.participantId, props.tripId)}
                            color="primary">
                        <Typography> {"Approve"} </Typography>
                    </Button>
                </Paper>
            </Grid>
        </React.Fragment>
    }

};

export default ApprovalOverview