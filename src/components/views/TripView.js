import Grid from '@material-ui/core/Grid'
import React from 'react'
import Typography from '@material-ui/core/Typography'
import GridList from '@material-ui/core/GridList'
import Avatar from '@material-ui/core/Avatar'
import Activites from '../helpers/Activites'
import Participants from '../helpers/Participants'
import Description from '../helpers/Description'
import {Link} from 'react-router-dom'
import axios from 'axios'
import JoinTripButton from '../utils/JoinTripButton'
import {AttachMoney, DateRange} from '@material-ui/icons'
import CustomButton from "../utils/CustomButton";
import {SERVER_HOST} from "../../commons";

class TripView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            joinTripState: '',
            isOwner: false
        }
        this.onClick = this.onClick.bind(this);
    }

    componentDidMount() {
        const {match: {params}} = this.props;

        axios.get(`http://${SERVER_HOST}:8080/api/trip/${params.id}`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            },
            params: {
                profileId: JSON.parse(localStorage.getItem('profileId'))
            }
        })
            .then(response => {
                console.log(response.data.isOwner)
                this.setState({
                    data: response.data.trip,
                    joinTripState: response.data.joinState,
                    isOwner: response.data.owner
                })

            })
    }

    onClick() {

        axios.post(`http://${SERVER_HOST}:8080/api/joinstate/participate`, null, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken'),
            },
            params: {
                profileId: JSON.parse(localStorage.getItem('profileId')),
                tripId: this.state.data.id,
            }
        })
            .then(response => {
                this.setState({
                    joinTripState: response.data,
                })

            })
    }

    render() {
        const {data, isOwner, joinTripState} = this.state;

        const {destinations} = data

        return (
            <Grid container spacing={2}>

                <Grid item>
                    <GridList cols={2.5}>
                        {data.img ?
                            <img src={data.img} alt={"alt"}/>
                            : null}
                    </GridList>
                </Grid>

                <Grid item container spacing={2} direction="column" justify="flex-start" alignItems="flex-start">

                    <Grid item container direction="column" justify="flex-start" alignItems="flex-start">
                        <Grid item>
                            <Typography variant={"h4"}> {data.title} </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant={"h5"}>
                                {
                                    destinations && (destinations.type === 'cities' ?
                                        destinations.places.map(place => `${place.name},${place.country.name} `)
                                        :
                                        destinations.countries.map(country => `${country.name} `))
                                }
                            </Typography>
                        </Grid>
                    </Grid>

                    <Grid item container direction="row">
                        <Grid item>
                            <Link
                                to={`/profile/${data.organizer ? data.organizer.id : null}`}>
                                <Avatar src={data.organizer ? data.organizer.img : null}/>
                            </Link>
                        </Grid>
                        <Grid>
                            <Typography>
                                {data.organizer && data.organizer.name} {data.organizer && data.organizer.surname}
                            </Typography>
                        </Grid>
                    </Grid>

                    <Description title="Description" desc={data.description}/>

                    <Grid item>
                        <Typography variant={"h6"}> <AttachMoney/> {data.budget} USD </Typography>
                    </Grid>
                    <Grid item>
                        <Typography> <DateRange/> {data.startDate} - {data.finishDate}</Typography>
                    </Grid>

                    <Activites title="Activities" interests={data.requirements}/>

                    <Participants participants={this.state.data.participants} sizeLimit={this.state.data.sizeLimit}/>

                    <Grid item container direction="row" justify="space-between">
                        {this.state.joinTripState !== 'APPROVED' && !isOwner &&
                        <JoinTripButton onClick={this.onClick} joinTripState={joinTripState}/>}
                        <Grid item>
                            <Typography> Displayed: {data.viewCounter} times </Typography>
                        </Grid>
                    </Grid>

                    {isOwner &&
                    <Link to={`/trip/${this.props.match.params.id}/edit`}>
                        <CustomButton msg="Edit Trip"/>
                    </Link>
                    }
                </Grid>
            </Grid>
        );
    }
};

export default TripView