import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import {withRouter} from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import SearchInput from '../forms/SearchInput'
import TripOverviews from '../helpers/TripOverviews'
import './SearchView.css'

const sortStrategies = ["createdAt::desc", "viewCounter::desc", "budget::asc", "dateFrom::asc"];

class SearchView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            value: 0,
            sortStrategy: "createdAt::desc",
            filterBy: {}
        }
    }

    handleChange = (event, newValue) => {
        this.setState({
            value: newValue,
            sortStrategy: sortStrategies[newValue]
        });
    };

    onFormSubmit = (e) => {
        e.preventDefault();

        const form = e.target;
        const formData = new FormData(form);
        let jsonObject = {};

        for (const [key, value] of formData.entries()) {
            jsonObject[key] = value;
        }

        console.log(jsonObject);

        this.setState({
            filterBy: jsonObject
        })

    };

    render() {
        return (
            <form onSubmit={this.onFormSubmit}>
                <Grid
                    container
                    spacing={2}
                    direction="row"
                    justify="space-between"
                    alignItems="flex-start">

                    <SearchInput/>
                    <Grid item xs={9} container spacing={2} direction="column" justify="space-between">

                        <Grid item>
                            <AppBar position="static">
                                <Tabs
                                    variant="fullWidth"
                                    value={this.state.value}
                                    onChange={this.handleChange}
                                    aria-label="nav tabs example"
                                >
                                    <Tab label="Newest"/>
                                    <Tab label="Most popular"/>
                                    <Tab label="Cheapest"/>
                                    <Tab label="Last minute"/>
                                </Tabs>
                            </AppBar>
                        </Grid>

                        <Grid item container direction='column' justify='flex-start' alignItems='stretch' spacing={5}>
                            <TripOverviews sortStrategy={this.state.sortStrategy} filterBy={this.state.filterBy}/>
                        </Grid>

                    </Grid>
                </Grid>
            </form>
        );
    }
};

export default withRouter(SearchView)