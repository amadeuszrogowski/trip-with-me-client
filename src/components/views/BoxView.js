import React from "react";
import axios from "axios";
import {withRouter} from "react-router-dom";
import ApprovalOverview from "../helpers/ApprovalOverview";
import Grid from "@material-ui/core/Grid";
import {SERVER_HOST} from "../../commons";

class BoxView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: []
        }

        this.onApprove = this.onApprove.bind(this);
        this.onDecline = this.onDecline.bind(this);
    }

    componentDidMount() {
        this.fetchMessages()
    }

    fetchMessages() {
        axios.get(`http://${SERVER_HOST}:8080/api/box/all`,  {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            },
            params: {
                profileId: JSON.parse(localStorage.getItem('profileId'))
            }
        })
            .then(response => {
                this.setState({
                    messages: response.data
                })
            });
    }

    onApprove(profileId, tripId) {
        this.onDecision('approve', profileId, tripId)
    }

    onDecline(profileId, tripId) {
        this.onDecision('decline', profileId, tripId)
    }

    onDecision(endpoint, profileId, tripId) {
        axios.post(`http://${SERVER_HOST}:8080/api/joinstate/${endpoint}`, null, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken'),
            },
            params: {
                profileId: profileId,
                tripId: tripId,
            }
        })
            .then(response => {
                this.props.history.go(0);
            })

    }

    render() {

        console.log(this.props)

        return <Grid item container spacing={3} direction={"column"}>
            {this.state.messages.map(data => {
                return <ApprovalOverview tripId={data.tripId}
                                         participantId={data.participantId}
                                         organizerId={data.organizerId}
                                         onApprove={this.onApprove}
                                         onDecline={this.onDecline}
                />
            })}
        </Grid>
    }

}

export default withRouter(BoxView)
