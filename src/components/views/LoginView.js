import React from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import {Link} from 'react-router-dom'

const LoginView = (props) => {

    return <form onSubmit={props.onSubmit}>
        <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="login"
            label="Login"
            name="usernameOrEmail"
            autoFocus
        />
        <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
        />
        <Button color="primary" type={"submit"}>
            Sign In
        </Button>
        <Link to={'/signup'}>
            <Button color="primary">
                Go to sign up page
            </Button>
        </Link>
    </form>
};

export default LoginView