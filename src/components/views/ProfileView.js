import React from 'react'
import Description from '../helpers/Description'
import Activites from '../helpers/Activites'
import Grid from '@material-ui/core/Grid'
import PersonalDetails from '../helpers/PersonalDetails'
import TravelDetails from '../helpers/TravelDetails'
import CustomButton from '../utils/CustomButton'
import {Link, withRouter} from 'react-router-dom'
import Typography from '@material-ui/core/Typography'
import BasicDetails from '../helpers/BasicDetails'
import ContactDetails from '../helpers/ContactDetails'
import axios from 'axios'
import TripOverview from '../helpers/TripOverview'
import {SERVER_HOST} from "../../commons";

class ProfileView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            profileId: this.props.match.params.id,
            tripsOrganized: [],
            profile: {},
            contactVisible: false
        }
    }

    componentDidMount() {
        axios.get(`http://${SERVER_HOST}:8080/api/profile/${this.state.profileId}`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            },
            params: {
                currentProfileId: JSON.parse(localStorage.getItem('profileId'))
            }
        })
            .then(response => {
                this.setState({
                    profile: response.data,
                    tripsOrganized: response.data.tripsOrganized,
                    contactVisible: response.data.contactVisible
                })
            });
    }

    // FIXME: you can navigate to current user profile when you are currently at another profile
    componentDidUpdate(prevProps, prevState, snapshot) {
        // this.componentDidMount()
    }

    isOwner() {
        return JSON.parse(localStorage.getItem('profileId')) === this.state.profile.id
    }

    render() {
        const {profile, tripsOrganized} = this.state

        return <Grid container spacing={2} direction="row" justify="space-between" alignItems="flex-start">
            <Grid item xs={3} container spacing={2} justify="center" alignItems="center">

                <BasicDetails
                    name={profile.name}
                    surname={profile.surname}
                    img={profile.img}
                    location={profile.location}
                />

                {this.state.contactVisible &&
                <ContactDetails
                    phoneNumber={profile.phoneNumber}
                    email={profile.email}
                    facebook={profile.facebook}
                />
                }

                {this.isOwner() &&
                <Link to={`/profile/${JSON.parse(localStorage.getItem('profileId'))}/edit`}>
                    <CustomButton msg="Edit Profile"/>
                </Link>
                }

            </Grid>


            <Grid item xs={9} container spacing={2} direction="column">
                <Grid item xs={12} container spacing={2} direction="row" justify="space-evenly" alignItems="stretch">

                    <PersonalDetails
                        dateOfBirth={profile.dateOfBirth}
                        gender={profile.gender}
                        course={profile.course}
                        university={profile.university}
                    />

                    <TravelDetails
                        languages={profile.languages}
                        countries={profile.countries}
                    />

                </Grid>
                <Grid item xs={12} container spacing={2}>

                    <Description title="About me" desc={profile.aboutMe}/>

                    <Activites title="Interests" interests={profile.interests}/>

                    <Grid item container direction={"column"}>
                        <Grid item>
                            <Typography variant={"h6"}> Current organized trips</Typography>
                        </Grid>
                        <Grid item container spacing={2}>
                            {tripsOrganized && tripsOrganized
                                .map(trip => <TripOverview key={trip.id} data={trip}/>)}
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    };
};

export default withRouter(ProfileView)