import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import InboxIcon from '@material-ui/icons/Inbox'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import PersonIcon from '@material-ui/icons/Person'
import {Link} from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import './TripBar.css'

const TripBar = (props) => {

    const currentProfileId = JSON.parse(localStorage.getItem('profileId'));
    const isAuth = JSON.parse(localStorage.getItem('isAuth'));

    return <AppBar position="static" className={"trip-bar"}>
        <Toolbar>
            <Grid item container direction="row" spacing={3} justify="space-between" alignItems="stretch">
                <Grid item>
                    <Link to={'/trips'}>
                        <Typography variant="h6"> TripWithMe </Typography>
                    </Link>
                </Grid>
                <Grid item>

                    {currentProfileId &&
                    <Link to={'/trips/create'}>
                        <Button className={'button-style'}>
                            <AddCircleIcon/>
                        </Button>
                    </Link>}

                    {isAuth &&
                    <Link to={`/box`}>
                        <Button className={'button-style'}>
                            <InboxIcon />
                        </Button>
                    </Link>}

                    {currentProfileId &&
                    <Link to={`/profile/${currentProfileId}`}>
                        <Button className={'button-style'}>
                            <PersonIcon/>
                        </Button>
                    </Link>}

                    {isAuth &&
                    <Link to={'/login'}>
                        <Button>
                            <Typography className={'button-text'}> Logout </Typography>
                        </Button>
                    </Link>}

                </Grid>
            </Grid>
        </Toolbar>
    </AppBar>
};

export default TripBar
