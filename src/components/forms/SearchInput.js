import React from 'react'
import Grid from '@material-ui/core/Grid'
import CustomButton from '../utils/CustomButton'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import {withRouter} from 'react-router-dom'
import MomentUtils from '@date-io/moment'
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers'
import moment from 'moment'
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import axios from "axios";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Input from "@material-ui/core/Input";
import Chip from "@material-ui/core/Chip";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import {SERVER_HOST} from "../../commons";

class SearchInput extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            text: '',
            searchType: 'places',
            countries: [],
            places: [],
            activities: [],
            fetchedPlaces: [],
            fetchedCountries: [],
            fetchedActivities: []
        }

    }

    componentDidMount() {
        axios.get(`http://${SERVER_HOST}:8080/api/utils`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            }
        })
            .then(response => {
                this.setState({
                    fetchedCountries: response.data.countries,
                    fetchedPlaces: response.data.places,
                    fetchedActivities: response.data.activities
                })
            });
    }

    handleChange = (e) => this.setState({
        ...this.state,
        [e.target.name]: e.target.value
    })

    handleChangeArray = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: [...e.target.value]
        })
    };

    handleStartDateChange = (date) => {
        console.log(date)
        const formattedDate = date.format('YYYY-MM-DD')
        this.setState({
            startDate: formattedDate
        })
    }

    handleFinishDateChange = (date) => {
        console.log(date)
        const formattedDate = date.format('YYYY-MM-DD')
        this.setState({
            finishDate: formattedDate
        })
    }

    getMenuProps() {
        return {
            PaperProps: {
                style: {
                    maxHeight: 48 * 4.5 + 8,
                    width: 250,
                },
            },
        };
    }

    createActivitiesMenuItems(collection) {
        return collection && collection.map(activity => <MenuItem key={activity}
                                                                 value={`${activity}`}> {`${activity}`} </MenuItem>);
    }

    createPlacesMenuItems(collection) {
        return collection && collection.map(destination => <MenuItem key={destination.place}
                                                   value={`${destination.place}`}> {`${destination.place}`} </MenuItem>);
    }

    createCountriesMenuItems(collection) {
        return collection && collection.map(country => <MenuItem key={country}
                                                                 value={`${country}`}> {`${country}`} </MenuItem>);
    }

    render() {
        console.log(this.state)

        const todayDate = moment()
        const yesterdayDate = moment().subtract(1, 'days')

        return <Grid item xs={3} container spacing={2} direction="column" justify="space-between"
                     alignItems="stretch">

            <Grid item>

                <RadioGroup
                    aria-label="destination-type"
                    name="searchType"
                    value={this.state.searchType}
                    onChange={this.handleChange}
                >
                    <FormControlLabel value="countries" control={<Radio color={'primary'}/>} label="Search by country" />
                    <FormControlLabel value="places" control={<Radio color={'primary'}/>} label="Search by city" />
                    <FormControlLabel value="text" control={<Radio color={'primary'}/>} label="Search by text" />
                </RadioGroup>

                {
                    this.state.searchType === 'places' &&

                    <FormControl>
                        <InputLabel shrink variant={'outlined'}>
                            Which destination?
                        </InputLabel>
                        <Select
                            id="demo-mutiple-chip"
                            multiple
                            name="places"
                            value={this.state.places}
                            onChange={this.handleChangeArray}
                            input={<Input id="select-multiple-chip"/>}
                            renderValue={selected =>
                                (
                                    <div>
                                        {selected.map(value => (
                                            <Chip key={value} label={value}/>
                                        ))}
                                    </div>
                                )
                            }
                            MenuProps={this.getMenuProps()}
                        >
                            {this.createPlacesMenuItems(this.state.fetchedPlaces)}
                        </Select>
                    </FormControl>
                }

                {
                    this.state.searchType === 'countries' &&

                    <FormControl>
                        <InputLabel shrink variant={'outlined'}>
                            Which country?
                        </InputLabel>
                        <Select
                            id="demo-mutiple-chip"
                            multiple
                            name="countries"
                            value={this.state.countries}
                            onChange={this.handleChangeArray}
                            input={<Input id="select-multiple-chip"/>}
                            renderValue={selected =>
                                (
                                    <div>
                                        {selected.map(value => (
                                            <Chip key={value} label={value}/>
                                        ))}
                                    </div>
                                )
                            }
                            MenuProps={this.getMenuProps()}
                        >
                            {this.createCountriesMenuItems(this.state.fetchedCountries)}
                        </Select>
                    </FormControl>
                }

                {
                    this.state.searchType === 'text' &&
                    <TextField
                    variant="filled"
                    margin="normal"
                    required
                    fullWidth
                    id="text"
                    placeholder={"e.g. Eiffel Tower, vine, museum"}
                    label="Search by text"
                    value={this.state.text}
                    onChange={this.handleChange}
                    name="text"
                    />

                }

                <Grid item container direction={'row'}>
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                        <Grid item xs={6}>
                            <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format="YYYY-MM-DD"
                                margin="normal"
                                id="startDate"
                                name="startDate"
                                label="From"
                                value={this.state.startDate || yesterdayDate}
                                onChange={this.handleStartDateChange}
                            />
                        </Grid>

                        <Grid item xs={6}>
                            <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format="YYYY-MM-DD"
                                margin="normal"
                                id="finishDate"
                                name="finishDate"
                                label="To"
                                value={this.state.finishDate || todayDate}
                                onChange={this.handleFinishDateChange}
                            />
                        </Grid></MuiPickersUtilsProvider>
                </Grid>

                <Grid item container direction={'row'}>

                    <Grid item xs={6}>
                        <TextField
                            variant="filled"
                            margin="normal"
                            required
                            fullWidth
                            id="minBudget"
                            label="Min budget"
                            name="minBudget"
                        />
                    </Grid>

                    <Grid item xs={6}>
                        <TextField
                            variant="filled"
                            margin="normal"
                            required
                            fullWidth
                            id="maxBudget"
                            label="Max budget"
                            name="maxBudget"
                        />
                    </Grid>
                </Grid>

                <Grid item container direction={'row'}>

                    <Grid item xs={6}>
                        <TextField
                            variant="filled"
                            margin="normal"
                            required
                            fullWidth
                            id="minPeople"
                            label="Min participants"
                            name="minPeople"
                        />
                    </Grid>

                    <Grid item xs={6}>
                        <TextField
                            variant="filled"
                            margin="normal"
                            required
                            fullWidth
                            id="maxPeople"
                            label="Max participants"
                            name="maxPeople"
                        />
                    </Grid>
                </Grid>

                <FormControl>
                    <InputLabel shrink variant={'outlined'}>
                        Choose activities
                    </InputLabel>
                    <Select
                        id="demo-mutiple-chip"
                        multiple
                        name="activities"
                        value={this.state.activities}
                        onChange={this.handleChangeArray}
                        input={<Input id="select-multiple-chip"/>}
                        renderValue={selected =>
                            (
                                <div>
                                    {selected.map(value => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </div>
                            )
                        }
                        MenuProps={this.getMenuProps()}
                    >
                        {this.createActivitiesMenuItems(this.state.fetchedActivities)}
                    </Select>
                </FormControl>

                <CustomButton msg="Search" type={'submit'}/>

            </Grid>
        </Grid>
    }
}

export default withRouter(SearchInput)