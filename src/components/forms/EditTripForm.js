import * as React from "react";
import axios from "axios";
import CustomButton from "../utils/CustomButton";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import ImageIcon from '@material-ui/icons/Image'
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Input from "@material-ui/core/Input";
import Chip from "@material-ui/core/Chip";
import MenuItem from "@material-ui/core/MenuItem";
import {SERVER_HOST} from "../../commons";


class EditTripForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tripId: this.props.match.params.id,
            fetchedActivities: [],
            activities: [],
            trip: {}
        }
    }

    componentDidMount() {
        axios.get(`http://${SERVER_HOST}:8080/api/trip/${this.state.tripId}`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            },
            params: {
                profileId: JSON.parse(localStorage.getItem('profileId'))
            }
        })
            .then(response => {
                this.setState({
                    trip: response.data.trip,
                    activities: response.data.trip.requirements.map(val => val.name)
                })
            });

        axios.get(`http://${SERVER_HOST}:8080/api/utils`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            },
        })
            .then(response => {
                this.setState({
                    fetchedActivities: response.data.activities,
                })
            });
    }

    handleChange = (e) => this.setState({
        ...this.state,
        trip: {
            ...this.state.trip,
            [e.target.name]: e.target.value
        }
    });

    handleChangeArrayGlobal = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: [...e.target.value]
        })
    };

    handleOnSubmit = (e) => {
        e.preventDefault();

        const form = e.target;
        const formData = new FormData(form);
        for (let name in formData.keys()) {
            formData.set(name, formData.get(name))
        }

        console.log('form data', ...formData);

        axios.post(`http://${SERVER_HOST}:8080/api/trip/${this.state.tripId}`,
            formData,
            {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('accessToken'),
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json'
                }
            }
        )
            .then(response => {
                    // use this.props.location.state.trips to get response data
                    this.props.history.push({
                        pathname: `/trip/${this.state.tripId}`,
                    });
                }
            )
            .catch(error => {
                console.log(error);
                return null
            });
    };

    createActivitiesMenuItems(collection) {
        return collection && collection.map(activity => <MenuItem key={activity}
                                                                  value={`${activity}`}> {`${activity}`} </MenuItem>);
    }

    getMenuProps() {
        return {
            PaperProps: {
                style: {
                    maxHeight: 48 * 4.5 + 8,
                    width: 250,
                },
            },
        };
    }

    render() {
        console.log(this.state)

        return (
            <form onSubmit={this.handleOnSubmit}>
                <Grid
                    container
                    spacing={2}
                    direction="row"
                    justify="space-between"
                    alignItems="flex-start">


                    <Grid item container direction="column">
                        <ImageIcon/>
                        <TextField
                            id="standard-multiline-flexible"
                            value={this.state.trip.img}
                            onChange={this.handleChange}
                            name="img"
                            margin="normal"
                        />
                    </Grid>

                    <Grid item container direction={"column"}>
                        <TextField
                            id="standard-multiline-flexible"
                            value={this.state.trip.title}
                            onChange={this.handleChange}
                            name="title"
                            margin="normal"
                        />
                    </Grid>

                    <Grid item container direction={"column"}>
                        <TextField
                            id="standard-multiline-flexible"
                            multiline
                            value={this.state.trip.description}
                            onChange={this.handleChange}
                            rowsMax="12"
                            name="description"
                            margin="normal"
                        />
                    </Grid>

                    <Grid item container direction={"column"}>
                        <TextField
                            id="standard-multiline-flexible"
                            value={this.state.trip.budget}
                            onChange={this.handleChange}
                            name="budget"
                            margin="normal"
                        />
                    </Grid>

                    <Grid item container direction="column">
                        <FormControl>
                            <InputLabel shrink variant={'outlined'}>
                                Activities
                            </InputLabel>
                            <Select
                                id="demo-mutiple-chip"
                                multiple
                                name="activities"
                                value={this.state.activities}
                                onChange={this.handleChangeArrayGlobal}
                                input={<Input id="select-multiple-chip"/>}
                                renderValue={selected =>
                                    (
                                        <div>
                                            {selected.map(value => (
                                                <Chip key={value} label={value}/>
                                            ))}
                                        </div>
                                    )
                                }
                                MenuProps={this.getMenuProps()}
                            >
                                {this.createActivitiesMenuItems(this.state.fetchedActivities)}
                            </Select>
                        </FormControl>
                    </Grid>

                    <CustomButton type="submit" msg="Submit"/>

                </Grid>
            </form>
        );
    }

}

export default EditTripForm