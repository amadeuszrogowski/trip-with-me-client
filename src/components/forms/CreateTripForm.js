import * as React from 'react'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import CustomButton from '../utils/CustomButton'
import axios from 'axios'
import './CreateTripForm.css'
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import moment from 'moment'
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Chip from "@material-ui/core/Chip";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import {SERVER_HOST} from "../../commons";

class CreateTripForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            description: "",
            title: "",
            budget: 2000,
            limit: 6,
            img: "",
            dateFrom: moment().subtract(1, 'days'),
            dateTo: moment(),
            destinationType: 'countries',
            countries: [],
            cities: [],
            activities: [],
            fetchedPlaces: [],
            fetchedCountries: [],
            fetchedActivities: []
        }
    }

    organizerId = JSON.parse(localStorage.getItem('profileId'));

    componentDidMount() {
        axios.get(`http://${SERVER_HOST}:8080/api/utils`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            }
        })
            .then(response => {
                console.log('personal info fields', response.data);
                this.setState({
                    fetchedCountries: response.data.countries,
                    fetchedPlaces: response.data.places,
                    fetchedActivities: response.data.activities
                })
            });
    }

    handleOnSubmit = (e) => {
        e.preventDefault();

        const form = e.target;
        const formData = new FormData(form);
        for (let name in formData.keys()) {
            formData.set(name, formData.get(name))
        }

        formData.set('organizerId', this.organizerId);
        formData.set('destinationType', this.state.destinationType);

        console.log(...formData)

        axios.post(`http://${SERVER_HOST}:8080/api/trip/new`,
            formData,
            {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('accessToken'),
                    'Content-Type': 'multipart/form-data',
                    Accept: 'application/json'
                }
            }
        )
            .then(response => {
                    this.props.history.push({pathname: `/trips`});
                }
            )
            .catch(error => {
                console.log(error);
                return null
            });
    };

    handleChange = (e) => this.setState({
        ...this.state,
        [e.target.name]: e.target.value
    });

    handleStartDateChange = (date) => {
        console.log(date)
        const formattedDate = date.format('YYYY-MM-DD')
        this.setState({
            dateFrom: formattedDate
        })
    }

    handleFinishDateChange = (date) => {
        console.log(date)
        const formattedDate = date.format('YYYY-MM-DD')
        this.setState({
            dateTo: formattedDate
        })
    }

    handleChangeArray = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: [...e.target.value]
        })
    };

    desc() {
        return <TextField
            className={'trip-input'}
            label={'Description'}
            id="standard-multiline-flexible"
            multiline
            value={this.state.description}
            onChange={this.handleChange}
            rows="4"
            rowsMax="12"
            name="description"
            margin="normal"
            placeholder={'Tell more about your trip and places you want to see.'}
        />
    }

    activities() {
        return (
            <FormControl>
                <InputLabel shrink variant={'outlined'}>
                    Choose activities
                </InputLabel>
                <Select
                    id="demo-mutiple-chip"
                    multiple
                    name="activities"
                    value={this.state.activities}
                    onChange={this.handleChangeArray}
                    input={<Input id="select-multiple-chip"/>}
                    renderValue={selected =>
                        (
                            <div>
                                {selected.map(value => (
                                    <Chip key={value} label={value}/>
                                ))}
                            </div>
                        )
                    }
                    MenuProps={this.getMenuProps()}
                >
                    {this.createActivitiesMenuItems(this.state.fetchedActivities)}
                </Select>
            </FormControl>
        );


    }

    createActivitiesMenuItems(collection) {
        return collection && collection.map(activity => {
            return <MenuItem key={activity} value={`${activity}`}> {`${activity}`} </MenuItem>
        });
    }

    tripDetails() {
        return <React.Fragment>
            <Grid item>
                <TextField
                    className={'trip-input'}
                    id="standard-multiline-flexible"
                    value={this.state.budget}
                    onChange={this.handleChange}
                    rowsMax="1"
                    name="budget"
                    margin="normal"
                    label={'Budget'}
                />
            </Grid>

            <MuiPickersUtilsProvider utils={MomentUtils}>
                <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    format="YYYY-MM-DD"
                    margin="normal"
                    id="dateFrom"
                    name="dateFrom"
                    label="From"
                    value={this.state.dateFrom}
                    onChange={this.handleStartDateChange}
                />
                <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    format="YYYY-MM-DD"
                    margin="normal"
                    id="dateTo"
                    name="dateTo"
                    label="To"
                    value={this.state.dateTo}
                    onChange={this.handleFinishDateChange}
                />
            </MuiPickersUtilsProvider>
        </React.Fragment>
    }

    basicDetails() {
        return <React.Fragment>
            <TextField
                id="standard-multiline-flexible"
                multiline
                value={this.state.title}
                onChange={this.handleChange}
                rowsMax="12"
                name="title"
                margin="normal"
                label={'Title'}
            />
        </React.Fragment>
    }


    createMenuItems(collection) {
        return collection.map(element => <MenuItem key={element} value={element}> {element} </MenuItem>);
    }

    getMenuProps() {
        return {
            PaperProps: {
                style: {
                    maxHeight: 48 * 4.5 + 8,
                    width: 250,
                },
            },
        };
    }

    createPlacesMenuItems(collection) {
        return collection.map(element => <MenuItem key={element.place}
                                                   value={`${element.place}`}> {`${element.place}`} </MenuItem>);
    }

    render() {
        return (
            <form onSubmit={this.handleOnSubmit}>
                <Grid container spacing={2}>
                    <TextField
                        id="standard-multiline-flexible"
                        multiline
                        value={this.state.img}
                        onChange={this.handleChange}
                        rowsMax="12"
                        name="img"
                        margin="normal"
                        label={'Photos'}
                    />

                    <Grid item container spacing={2} direction="column" justify="flex-start" alignItems="flex-start">

                        {this.basicDetails()}

                        <RadioGroup
                            aria-label="destination-type"
                            name="destinationType"
                            value={this.state.destinationType}
                            onChange={this.handleChange}
                        >
                            <FormControlLabel value="countries" control={<Radio color={'primary'}/>} label="Countries"/>
                            <FormControlLabel value="cities" control={<Radio color={'primary'}/>} label="Cities"/>
                        </RadioGroup>

                        {
                            this.state.destinationType === 'countries' &&
                            <FormControl>
                                <Select
                                    id="demo-mutiple-chip"
                                    multiple
                                    required
                                    name="countries"
                                    value={this.state.countries}
                                    onChange={this.handleChangeArray}
                                    input={<Input id="select-multiple-chip"/>}
                                    renderValue={selected =>
                                        (
                                            <div>
                                                {selected.map(value => (
                                                    <Chip key={value} label={value}/>
                                                ))}
                                            </div>
                                        )
                                    }
                                    MenuProps={this.getMenuProps()}
                                >
                                    {this.createMenuItems(this.state.fetchedCountries)}
                                </Select>
                            </FormControl>
                        }

                        {
                            this.state.destinationType === 'cities' &&

                            <FormControl>
                                <Select
                                    id="demo-mutiple-chip"
                                    multiple
                                    required
                                    name="cities"
                                    value={this.state.cities}
                                    onChange={this.handleChangeArray}
                                    input={<Input id="select-multiple-chip"/>}
                                    renderValue={selected =>
                                        (
                                            <div>
                                                {selected.map(value => (
                                                    <Chip key={value} label={value}/>
                                                ))}
                                            </div>
                                        )
                                    }
                                    MenuProps={this.getMenuProps()}
                                >
                                    {this.createPlacesMenuItems(this.state.fetchedPlaces)}
                                </Select>
                            </FormControl>
                        }

                        {this.desc()}

                        {this.tripDetails()}

                        {this.activities()}

                        <TextField
                            id="standard-multiline-flexible"
                            multiline
                            value={this.state.limit}
                            onChange={this.handleChange}
                            rowsMax="12"
                            name="limit"
                            margin="normal"
                            label={'Participants'}
                        />

                        <CustomButton type="submit" msg="Submit"/>
                    </Grid>
                </Grid>
            </form>
        );
    }
}

export default CreateTripForm