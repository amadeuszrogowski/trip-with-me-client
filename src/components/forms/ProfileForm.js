import * as React from 'react'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import WorkIcon from '@material-ui/icons/Work'
import SchoolIcon from '@material-ui/icons/School'
import WcIcon from '@material-ui/icons/Wc'
import CakeIcon from '@material-ui/icons/Cake'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import LanguageIcon from '@material-ui/icons/Language'
import PublicIcon from '@material-ui/icons/Public'
import PhoneIcon from '@material-ui/icons/Phone'
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail'
import WebIcon from '@material-ui/icons/Web'
import PersonIcon from '@material-ui/icons/Person'
import ImageIcon from '@material-ui/icons/Image'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import axios from 'axios'
import {withRouter} from 'react-router-dom'
import CustomButton from '../utils/CustomButton'
import Input from '@material-ui/core/Input'
import Chip from '@material-ui/core/Chip'
import InputLabel from "@material-ui/core/InputLabel";
import {SERVER_HOST} from "../../commons";

class ProfileForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            profileId: this.props.match.params.id,
            profile: {},
            genders: [],
            courses: [],
            universities: [],
            countries: [],
            languages: [],
            places: [],
            activities: [],
            fetchedActivities: []
        }
    }

    componentDidMount() {
        axios.get(`http://${SERVER_HOST}:8080/api/profile/${this.state.profileId}`,{
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            },
            params: {
                currentProfileId: JSON.parse(localStorage.getItem('profileId'))
            }
        })
            .then(response => {
                console.log('profile response', response.data);
                this.setState({
                    profile: response.data,
                    activities: response.data.interests
                })
            });

        axios.get(`http://${SERVER_HOST}:8080/api/utils`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('accessToken')
            },
        })
            .then(response => {
                this.setState({
                    genders: response.data.genders,
                    courses: response.data.courses,
                    universities: response.data.universities,
                    countries: response.data.countries,
                    languages: response.data.languages,
                    places: response.data.places,
                    fetchedActivities: response.data.activities
                })
            });
    }

    handleChange = (e) => this.setState({
        ...this.state,
        profile: {
            ...this.state.profile,
            [e.target.name]: e.target.value
        }
    });

    handleChangeArray = (e) => {
        this.setState({
            ...this.state,
            profile: {
                ...this.state.profile,
                [e.target.name]: [...e.target.value]
            }
        })
    };

    handleChangeArrayGlobal = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: [...e.target.value]
        })
    };

    handleOnSubmit = (e) => {
        e.preventDefault();

        const form = e.target;
        const formData = new FormData(form);
        for (let name in formData.keys()) {
            formData.set(name, formData.get(name))
        }

        axios.post(`http://${SERVER_HOST}:8080/api/profile/${this.state.profileId}`,
            formData,
            {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('accessToken'),
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json'
                }
            }
        )
            .then(response => {
                    // use this.props.location.state.trips to get response data
                    this.props.history.push({
                        pathname: `/profile/${this.state.profileId}`,
                        state: {
                            trips: response.data
                        }
                    });
                }
            )
            .catch(error => {
                console.log(error);
                return null
            });

    };

    createPlacesMenuItems(collection) {
        return collection.map(element =>
            <MenuItem key={element.place} value={`${element.place},${element.country}`}> {`${element.place},${element.country}`} </MenuItem>);
    }

    createMenuItems(collection) {
        return collection.map(element => <MenuItem key={element} value={element}> {element} </MenuItem>);
    }

    getMenuProps() {
        return {
            PaperProps: {
                style: {
                    maxHeight: 48 * 4.5 + 8,
                    width: 250,
                },
            },
        };
    }

    basicDetails() {
        return <Paper>
            <Grid item>
                <Typography variant={"h6"}> Basic details </Typography>
            </Grid>
            <Grid item container direction="column" justify="flex-start" alignItems="flex-start" spacing={1}>
                <Grid item container direction="row">
                    <ImageIcon/>
                    <TextField
                        id="standard-multiline-flexible"
                        value={this.state.profile && this.state.profile.img}
                        onChange={this.handleChange}
                        name="img"
                        margin="normal"
                    />
                </Grid>
                <Grid item container direction="row">
                    <PersonIcon/>
                    <TextField
                        id="standard-multiline-flexible"
                        value={this.state.profile && this.state.profile.name}
                        onChange={this.handleChange}
                        name="name"
                        margin="normal"
                    />
                    <TextField
                        id="standard-multiline-flexible"
                        value={this.state.profile && this.state.profile.surname}
                        onChange={this.handleChange}
                        name="surname"
                        margin="normal"
                    />
                </Grid>
                <Grid item container direction="row">
                    <LocationOnIcon/>
                    <Select
                        id="demo-mutiple-chip"
                        name="location"
                        value={this.state.profile && this.state.profile.location}
                        onChange={this.handleChange}
                        input={<Input id="select-multiple-chip"/>}
                        renderValue={selected => <Chip key={selected} label={selected}/>}
                        MenuProps={this.getMenuProps()}
                    >
                        {this.createPlacesMenuItems(this.state.places || [])}
                    </Select>
                </Grid>
            </Grid>
        </Paper>
    }

    contactDetails() {
        return <Paper>
            <Grid item>
                <Typography variant={"h6"}> Contact details </Typography>
            </Grid>
            <Grid item container direction="column" justify="flex-start" alignItems="flex-start" spacing={1}>
                <Grid item container direction="row">
                    <PhoneIcon/>
                    <TextField
                        id="standard-multiline-flexible"
                        value={this.state.profile && this.state.profile.phoneNumber}
                        onChange={this.handleChange}
                        name="phoneNumber"
                        margin="normal"
                    />
                </Grid>
                <Grid item container direction="row">
                    <AlternateEmailIcon/>
                    <TextField
                        id="standard-multiline-flexible"
                        value={this.state.profile && this.state.profile.email}
                        onChange={this.handleChange}
                        name="email"
                        margin="normal"
                    />
                </Grid>
                <Grid item container direction="row">
                    <WebIcon/>
                    <TextField
                        id="standard-multiline-flexible"
                        value={this.state.profile && this.state.profile.facebook}
                        onChange={this.handleChange}
                        name="facebook"
                        margin="normal"
                    />
                </Grid>
            </Grid>
        </Paper>;
    }

    aboutMe() {
        return (
            <React.Fragment>
                <Grid item>
                    <Typography variant={"h6"}> About Me </Typography>
                </Grid>
                <Paper>
                    <Grid item>
                        <TextField
                            id="standard-multiline-flexible"
                            multiline
                            value={this.state.profile && this.state.profile.aboutMe}
                            onChange={this.handleChange}
                            rowsMax="12"
                            name="aboutMe"
                            margin="normal"
                        />
                    </Grid>
                </Paper>
            </React.Fragment>
        );
    }

    activities() {
        return (
            <Grid item container direction="column">
                <FormControl>
                    <InputLabel shrink variant={'outlined'}>
                        Interests
                    </InputLabel>
                    <Select
                        id="demo-mutiple-chip"
                        multiple
                        name="activities"
                        value={this.state.activities}
                        onChange={this.handleChangeArrayGlobal}
                        input={<Input id="select-multiple-chip"/>}
                        renderValue={selected =>
                            (
                                <div>
                                    {selected.map(value => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </div>
                            )
                        }
                        MenuProps={this.getMenuProps()}
                    >
                        {this.createActivitiesMenuItems(this.state.fetchedActivities)}
                    </Select>
                </FormControl>
            </Grid>
        );
    }

    createActivitiesMenuItems(collection) {
        return collection && collection.map(activity => <MenuItem key={activity}
                                                                  value={`${activity}`}> {`${activity}`} </MenuItem>);
    }

    travelDetails() {
        return <Paper>
            <Grid item>
                <Typography variant={"h6"}> Travel details </Typography>
            </Grid>
            <Grid item>
                <FormControl>
                    <LanguageIcon/>
                    <Select
                        id="demo-mutiple-chip"
                        multiple
                        name="languages"
                        value={this.state.profile && (this.state.profile.languages || [])}
                        onChange={this.handleChangeArray}
                        input={<Input id="select-multiple-chip"/>}
                        renderValue={selected =>
                            (
                                <div>
                                    {selected.map(value => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </div>
                            )
                        }
                        MenuProps={this.getMenuProps()}
                    >
                        {this.createMenuItems(this.state.languages || [])}
                    </Select>
                </FormControl>
            </Grid>
            <Grid item>
                <FormControl>
                    <PublicIcon/>
                    <Select
                        id="demo-mutiple-chip"
                        multiple
                        name="countries"
                        value={this.state.profile && (this.state.profile.countries || [])}
                        onChange={this.handleChangeArray}
                        input={<Input id="select-multiple-chip"/>}
                        renderValue={selected =>
                            (
                                <div>
                                    {selected.map(value => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </div>
                            )
                        }
                        MenuProps={this.getMenuProps()}
                    >
                        {this.createMenuItems(this.state.countries)}
                    </Select>
                </FormControl>
            </Grid>
        </Paper>;
    }

    personalDetails() {
        return <Paper>

            <Grid item container>
                <Typography variant={"h6"}> Personal details </Typography>
            </Grid>

            <Grid item container direction="row">
                <CakeIcon/>
                <TextField
                    id="standard-multiline-flexible"
                    value={this.state.profile && this.state.profile.dateOfBirth}
                    onChange={this.handleChange}
                    name="dateOfBirth"
                    margin="normal"
                />
            </Grid>

            <Grid item container direction="row">
                <WcIcon/>
                <FormControl>
                    <Select
                        value={this.state.profile && this.state.profile.gender}
                        onChange={this.handleChange}
                        displayEmpty
                        name="gender">
                        {this.createMenuItems(this.state.genders || [])}
                    </Select>
                </FormControl>
            </Grid>

            <Grid item container direction="row">
                <WorkIcon/>
                <FormControl>
                    <Select
                        value={this.state.profile && this.state.profile.course}
                        onChange={this.handleChange}
                        displayEmpty
                        name="course">
                        {this.createMenuItems(this.state.courses || [])}
                    </Select>
                </FormControl>
            </Grid>

            <Grid item container direction="row">
                <SchoolIcon/>
                <FormControl>
                    <Select
                        value={this.state.profile && this.state.profile.university}
                        onChange={this.handleChange}
                        displayEmpty
                        name="university">
                        {this.createMenuItems(this.state.universities || [])}
                    </Select>
                </FormControl>
            </Grid>
        </Paper>;
    }

    render() {
        console.log('in render method', this.state)

        return (
            <form onSubmit={this.handleOnSubmit}>
                <Grid
                    container
                    spacing={2}
                    direction="row"
                    justify="space-between"
                    alignItems="flex-start">


                    <Grid item xs={3} container spacing={2}>

                        <Grid item container>
                            {this.basicDetails()}
                        </Grid>

                        <Grid item container>
                            {this.contactDetails()}
                        </Grid>

                    </Grid>

                    <Grid item xs={9} container spacing={2} direction="column">

                        <Grid item xs={9} container spacing={2} direction="row"
                              justify="space-evenly"
                              alignItems="stretch">

                            <Grid item container direction="column">
                                {this.personalDetails()}
                            </Grid>

                            <Grid item container direction="column">
                                {this.travelDetails()}
                            </Grid>

                        </Grid>

                        <Grid item xs={9} container spacing={2}>

                            <Grid item container direction={"column"}>
                                {this.aboutMe()}
                            </Grid>

                            <Grid item container direction={"column"}>
                                {this.activities()}
                            </Grid>

                            <CustomButton type="submit" msg="Submit"/>
                        </Grid>

                    </Grid>

                </Grid>
            </form>
        );
    }
}

export default withRouter(ProfileForm)